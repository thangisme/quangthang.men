---
layout: post
title: Ngày 1 - Series 30 ngày tự học tiếng Anh
image: /assets/featured_imgs/30s-days-english.png
description: Ngày đầu tiên trong chuỗi 30 ngày tự học tiếng Anh
---

![Cover image](/assets/featured_imgs/30-days-english.png)
Sau những lần tự dặn lòng phải chăm chỉ học tiếng Anh nhưng do lười quá nên thôi hoãn lại,
hôm nay mình chính thức bắt đầu thử thách tự học tiếng Anh trong ít nhất 30 ngày =)

## Hôm nay học được những gì?
Bây giờ đang gần 12h48, trời nóng thật :(. Thôi thì vào trang [English test store](https://englishteststore.net) làm một bài nghe cơ bản vậy :3.

*12h59*, đã làm xong một [bài nghe](https://englishteststore.net/index.php?option=com_content&view=article&id=10769:english-listening-comprehension-conversation-hard-level-test-01&catid=29&Itemid=800), đạt 9/10. Bài này ngắn lên làm thêm một bài nữa


*13h08*, hoàn thành [bài nghe miêu tả ảnh số 1](https://englishteststore.net/index.php?option=com_content&view=article&id=12323:listening-comprehension-picture-description-test-01&catid=22&Itemid=360), được có 8/10 :(. Không học nữa :v

--- 

Bây giờ là hơn 8h tối, mới ăn cơm xong 😁. 
Giờ học Unit 3 về thì quá khứ với hiện tại hoàn thành 
trong cuốn Advanced english grammar in use. 

Đã học xong, tiếp tục vào English Test Store làm thêm 2
bài về mệnh đề A/an/the, thêm một bài nghe và một bài đọc.
Nhưng không học được gì về phần speaking với pronunciation ...
Nói chung chọn level thường lên cũng không có gì khó lắm 😌.
Cơ mà trang này làm có vẻ không ổn lắm, phải tìm trang khác thôi 😢.

Đã thế mãi mà vẫn không tìm ra khoá nào hay để học 😑.
Nên học thêm cả IELTS nữa không nhỉ 😶?[^fn-ielts]

Mới ngày đầu mà có vẻ không tiến triển lắm :v

[^fn-ielts]: Kiến thức cơ bản còn chưa vững mà cứ thích mơ xa vời  @@
